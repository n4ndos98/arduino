//--- Bibliotecas ---
#include "HX711.h"
#include "LiquidCrystal_I2C.h"
#include <RTClib.h> 

//--- Constantes ---
#define DT A1
#define SCK A0

//--- Variáveis para controle do Driver ---
const int motorB1  = 5;       
const int motorB2  = 6;       

//--- Variavéis para controle do horário RTC ---
int Hor;              
int Min;              
int Sec;             
int Data;             

//--- Objetos de controle ---
HX711 escala;
LiquidCrystal_I2C lcd(0x27, 16, 2);
RTC_DS1307 rtc;

//--- Paramêtros de configuração --- 
void setup() {

  //--- Iniciação do RTC e HX711 ---
  rtc.begin();
  escala.begin (DT, SCK);
 
  //--- Iniciação do LCD
  lcd.init();
  lcd.backlight();

  //--- Configuração dos pinos do Driver
  pinMode(motorB1, OUTPUT);   
  pinMode(motorB2, OUTPUT);  

  //--- Ajuste Manual ou Automático ---
  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));   //Automático
  //rtc.adjust(DateTime(2020, 04, 16, 10, 00, 00)); //Manual

  //--- Iniciação da leitura do HX711 ---
  Serial.begin(9600);
  Serial.print("Leitura do Valor ADC:  ");
  Serial.println(escala.read());   // Aguada até o dispositivo estar pronto
  Serial.println("Nao coloque nada na balanca!");
  Serial.println("Iniciando...");
  escala.set_scale(397930.55);     // Substituir o valor encontrado para escala
  escala.tare(20);                // O peso é chamado de Tare.
  Serial.println("Insira o item para Pesar");
}
 
void loop() {

  //--- Controle do LCD com HX711 ---
  lcd.setCursor(4, 0);
  lcd.print("USINAINFO");
  lcd.setCursor(0, 1);
  lcd.print("Peso: ");
  lcd.print(escala.get_units(20), 3);
  lcd.println(" kg  ");

  //--- Controle do RTC ---
  Hor = rtc.now().hour();       // Verifica a Hora
  Min = rtc.now().minute();     // Verifica os Minutos
  Sec = rtc.now().second();     // Verifica os Segundos
  Data = rtc.now().day();       // Verifica o Dia
 
  // Verifica o horário e se o mesmo for igual à 12:00:00
  if ( Hor == 12 &&  Min == 00 && Sec == 00) {
    analogWrite(motorB1, 255);
    analogWrite(motorB2, 0);
    delay(3000);
    analogWrite(motorB1, 0);
    analogWrite(motorB2, 255);
    delay(2000);
    analogWrite(motorB1, 255);
    analogWrite(motorB2, 0);
    delay(3000);
    analogWrite(motorB1, 0);
    analogWrite(motorB2, 0);
  }
 
  // Verifica o horário e se o mesmo for igual à 18:00:00
  if ( Hor == 18 &&  Min == 00 && Sec == 00) {
    analogWrite(motorB1, 255);
    analogWrite(motorB2, 0);
    delay(3000);
    analogWrite(motorB1, 0);
    analogWrite(motorB2, 255);
    delay(2000);
    analogWrite(motorB1, 255);
    analogWrite(motorB2, 0);
    delay(3000);
    analogWrite(motorB1, 0);
    analogWrite(motorB2, 0);
  }
 
  delay(1000);                      // Aguarda 1 segundo e reinicia
}
